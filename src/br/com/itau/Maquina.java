package br.com.itau;

import javax.swing.*;
import java.util.*;

public class Maquina {

    private static List<Simbolos> slots = new ArrayList<>();
    private static List<Simbolos> simbolos = Arrays.asList(Simbolos.values());

    public static void mostrarMenu() {
            int opcao = 0;
            do {
                String opcaoMenu = JOptionPane.showInputDialog(null, "\n1 - Jogar \n2 - Sair \n ");
                try {
                    opcao = Integer.parseInt(opcaoMenu);
                    if (opcao == 1) {
                        jogar();
                    } else if (opcao > 2) {
                        JOptionPane.showMessageDialog(null, "Opção inválida!", "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                } catch ( Exception ex) {
                    JOptionPane.showMessageDialog(null, "Opção inválida!", "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            } while (opcao != 2);
    }

    public static void jogar() {
            slots.clear();
            sortearSimbolos();
            mostrarPontuacao();
    }

    public static void sortearSimbolos() {
        Random random = new Random();;
        for (int i = 0; i < 3; i++) {
            slots.add(simbolos.get(random.nextInt(4)));
        }
    }

    public static void mostrarPontuacao() {
        String resultado = "\n";
        int pontuacao = 0;

        for (Simbolos simbolo: slots) {
            resultado = resultado + simbolo.getName() + " | ";
            pontuacao = pontuacao + simbolo.getPontuacao();
            if (slots.get(0).getName().equals(slots.get(1).getName()) && slots.get(1).getName().equals(slots.get(2).getName())) {
                pontuacao = pontuacao + 100;
            }
        }
        JOptionPane.showConfirmDialog(null, resultado + "\nPontuação total: " + pontuacao + "\n ", "RESULTADO", JOptionPane.OK_OPTION);
    }

}
