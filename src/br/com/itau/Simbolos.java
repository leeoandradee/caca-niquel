package br.com.itau;

public enum Simbolos {

    BANANA("banana", 10),
    FRAMBOESA("framboesa", 50),
    MOEDA("moeda", 100),
    SETE("sete", 300);

    private String name;
    private int pontuacao;

    Simbolos(String name, int pontuacao) {
        this.name = name;
        this.pontuacao = pontuacao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
}
